package menu.actions;

import constructor.Context;
import entity.Ship;
import menu.Action;

public class ViewQueueAction implements Action {
    @Override
    public void execute() {
        System.out.println("Your ships:");
        if (Context.getInstance().waitingShips.size() == 0){
            System.out.println("You haven't waiting ships");
        }else{
            for (Ship ships : Context.getInstance().waitingShips){
                System.out.println("id: " + ships.getId() + " water: " + ships.getCapacity());
            }
        }
        System.out.println();
    }
}
