package menu.actions;

import constructor.Context;
import entity.Ship;
import menu.Action;

import java.util.Scanner;

public class LoadShipAction implements Action {
    @Override
    public void execute() {
        System.out.print("input id of ship, witch should be loaded> ");
        Scanner scanner = new Scanner(System.in);
        int searchedId = scanner.nextInt();

        for (Ship ship : Context.getInstance().waitingShips){
            if (ship.getId() == searchedId) {
                Context.getInstance().port.getShipsInside().add(ship);
                Context.getInstance().waitingShips.remove(ship);
                return;
            }else{
                break;
            }
        }
        System.out.println("bup..error!");
    }
}
