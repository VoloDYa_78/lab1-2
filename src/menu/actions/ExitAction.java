package menu.actions;

import menu.Action;

public class ExitAction implements Action {

    @Override
    public void execute() {
        System.exit(0);
    }
}
