package menu.actions;

import constructor.Context;
import menu.Action;

public class CheckWaterAction implements Action {
    @Override
    public void execute() {
        System.out.println("Water in port: " + Context.getInstance().port.getWaterCapacity());
    }
}
