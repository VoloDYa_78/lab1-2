package menu.actions;

import constructor.Context;
import entity.Ship;
import menu.Action;

import java.util.ArrayList;
import java.util.Scanner;

public class FillShipAction implements Action {

    @Override
    public void execute() {
        System.out.print("input capacity of ship> ");
        Scanner scanner = new Scanner(System.in);
        int capacity = scanner.nextInt();

        ArrayList<Ship> queueShips = Context.getInstance().waitingShips;
        if (capacity == 450 || capacity == 900 || capacity == 1350 || capacity == 1800
                || capacity == 1000 || capacity == 2000 || capacity == 1450 || capacity == 1900) {
            queueShips.get(queueShips.size()-1).setCapacity(capacity);
        } else {
            queueShips.get(queueShips.size()-1).setCapacity(0);
        }
    }
}
