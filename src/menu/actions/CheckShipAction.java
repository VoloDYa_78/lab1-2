package menu.actions;

import constructor.Context;
import menu.Action;

import java.util.Scanner;

public class CheckShipAction implements Action {
    @Override
    public void execute() {
        System.out.println("input id> ");
        Scanner scanner = new Scanner(System.in);
        int searchedId = scanner.nextInt();
        Context.getInstance().waitingShips.forEach((ship -> {
            if (ship.getId() == searchedId) {
                System.out.println("id: " + ship.getId() + " water: " + ship.getCapacity());
                return;
            } else {
                System.out.println("Sorry, it's absent(");
                return;
            }
        }));
    }
}
