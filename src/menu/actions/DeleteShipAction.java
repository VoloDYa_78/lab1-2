package menu.actions;

import constructor.Context;
import entity.Port;
import entity.Ship;
import menu.Action;

import java.util.Scanner;

public class DeleteShipAction implements Action {
    @Override
    public void execute() {
        if (Context.getInstance().port.getShipsInside().size() != 0) {
            System.out.print("inp id> ");
            Scanner scanner = new Scanner(System.in);
            int searchedId = scanner.nextInt();


            for (Ship ship : Context.getInstance().port.getShipsInside()) {
                if (ship.getId() == searchedId) {
                    Context.getInstance().port.setWaterCapacity(Context.getInstance().port.getWaterCapacity() + ship.getCapacity());
                    Context.getInstance().port.getShipsInside().remove(ship);
                    return;
                }
            }
            System.out.println("bup..bup..error");
        } else {
            System.out.println("You haven't ships in Port");

        }
    }
}