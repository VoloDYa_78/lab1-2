package menu.actions;

import constructor.Context;
import entity.Ship;
import menu.Action;

public class ViewShipsAction implements Action {
    @Override
    public void execute() {
        if ( Context.getInstance().port.getShipsInside().size()==0){
            System.out.println("You haven't ships in port");
        }else{
            Context.getInstance().port.getShipsInside().forEach(ship -> System.out.println(ship.getId()));
        }
    }
}
