package menu.actions;

import constructor.Context;
import entity.Ship;
import menu.Action;

import java.util.Random;
import java.util.Scanner;

public class CreateShipAction implements Action {
    @Override
    public void execute() {
        Context.getInstance().waitingShips.add(new Ship());
    }
}
