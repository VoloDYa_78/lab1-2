package menu;

import java.util.Scanner;

public class Navigator {

    private Menu currentMenu;

    public void setCurrentMenu(Menu currentMenu) {
        this.currentMenu = currentMenu;
    }

    public Menu getCurrentMenu() {
        return currentMenu;
    }

    private static Navigator instance;

    private Navigator() {
    }

    public static Navigator getInstance() {
        if (instance == null) {
            instance = new Navigator();
        }
        return instance;
    }

    public void printMenu() {
        for (MenuItem menuItem : instance.getCurrentMenu().getMenuItems()) {
            System.out.println(menuItem.getKey() + menuItem.getTitle());
        }
    }
    public void printSubMenu(MenuItem menuItem) {
        String key = menuItem.getSubMenu().getKey();
        String title = menuItem.getSubMenu().getTitle();
        System.out.println(key + title);
    }

    public void navigate() {
        String key = userChoise();
        MenuItem menuChoice = null;

        for (MenuItem menuItem : instance.getCurrentMenu().getMenuItems()) {
            if (key.equals(menuItem.getKey())) {
                menuChoice = menuItem;
                break;
            }
        }

        if (menuChoice != null) {
            menuChoice.executeAction();
            MenuItem subMenu = menuChoice.getSubMenu();

            if (subMenu != null) {
                printSubMenu(menuChoice);
                subNavigate(subMenu);
            }
        } else {
            System.out.println("Bup..Bup...\nSorry, u got error ^_^");
        }
    }

    private void subNavigate(MenuItem subMenu) {
        //Потом вместо "MenuItem menuItem" можно вставить список
        //таких и чутокизменить метод (если подменю станет больше)
        String key = userChoise();
        if (key.equals(subMenu.getKey())) {
            subMenu.executeAction();
        } else {
            System.out.println("Bup..Bup...\nSorry, u got error ^_^");
        }

    }

    private String userChoise() {
        System.out.print("inp > ");
        Scanner in = new Scanner(System.in);
        return in.next();
    }
}
