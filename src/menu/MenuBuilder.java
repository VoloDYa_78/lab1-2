package menu;

import menu.actions.*;

import java.util.List;

public class MenuBuilder {

    public Menu mainMenu;

    public Menu getMainMenu() {
        return mainMenu;
    }

    private static MenuBuilder instance;

    private MenuBuilder(){
    }

    public static MenuBuilder getInstance(){
        if(instance==null){
            instance = new MenuBuilder();
        }
        return instance;
    }

    public void buildMenu(){
        mainMenu = new Menu ("Main");

        mainMenuInit();
        //Set menu in Navigator
    }

    /*
     1. Посмотреть сколько воды в порту
     2. Посмотреть список кораблей в порту
     3. Удалить корабль из порта (вода остаётся в порту)
     4. Создать корабль
        -> наполнить корабль контейнерами с водой
     5. Посмотреть список кораблей, ожидающих прибытия в порт
        -> посмотреть инормацию по конкретному кораблю
     6. Загрузить корабль в порт
     7. Выйти из программы
 */



    public void mainMenuInit(){
        mainMenu.getMenuItems().add(new MenuItem("1", " Посмотреть сколько воды в порту", new CheckWaterAction(), null, null, null));
        mainMenu.getMenuItems().add(new MenuItem("2", " Посмотреть список кораблей в порту", new ViewShipsAction(), null, null, null));
        mainMenu.getMenuItems().add(new MenuItem("3", " Удалить корабль из порта (вода остаётся в порту)", new DeleteShipAction(), null, null, null));
        mainMenu.getMenuItems().add(new MenuItem("4", " Создать корабль", new CreateShipAction(), null, null,
                                    new MenuItem("4.1", " наполнить корабль контейнерами с водой", new FillShipAction(),null,null,null)));
        mainMenu.getMenuItems().add(new MenuItem("5", " Посмотреть список кораблей, ожидающих прибытия в порт", new ViewQueueAction(), null, null,
                                    new MenuItem("5.1"," посмотреть инормацию по конкретному кораблю", new CheckShipAction(),null,null,null)));
        mainMenu.getMenuItems().add(new MenuItem("6", " Загрузить корабль в порт", new LoadShipAction(), null, null, null));
        mainMenu.getMenuItems().add(new MenuItem("7", " Выйти из программы", new ExitAction(), null, null, null));
    }
}
