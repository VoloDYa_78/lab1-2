package menu;

import java.util.ArrayList;
import java.util.List;

public class Menu {

    public String getName() {
        return name;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    private String name;
    private List<MenuItem> menuItems = new ArrayList<>();

    public Menu(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public Menu(String name) {
        this.name = name;
    }

    public Menu(List<MenuItem> menuItems, String name) {
        this.menuItems = menuItems;
        this.name = name;
    }
}
