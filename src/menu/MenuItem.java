package menu;

public class MenuItem {

    private String key;

    private String title;
    private Action action;
    private Menu nextMenu;
    private Menu previousMenu;

    private MenuItem subMenu;

    public String getKey() {
        return key;
    }

    public String getTitle() {
        return title;
    }

    public MenuItem getSubMenu() {
        return subMenu;
    }

    public MenuItem() {
    }

    public MenuItem(String key, String title, Action action, Menu nextMenu, Menu previousMenu, MenuItem subMenu) {
        this.key = key;
        this.title = title;
        this.action = action;
        this.nextMenu = nextMenu;
        this.previousMenu = previousMenu;
        this.subMenu = subMenu;
    }

    public void executeAction() {
        action.execute();
    }
}
