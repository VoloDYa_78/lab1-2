package menu;

public class MenuController {

    private static MenuController instance;

    private MenuBuilder builder;
    private Navigator navigator;

    MenuController(){
        this.builder = MenuBuilder.getInstance();
        this.builder.buildMenu();
        this.navigator = Navigator.getInstance();
        this.navigator.setCurrentMenu(this.builder.getMainMenu());
    }

    public static MenuController getInstance(){
        if (instance == null){
            instance = new MenuController();
        }
        return instance;
    }

    public void run(){
        while (true){
            navigator.printMenu();
            navigator.navigate();
        }
    }
}
