package com.company;

import constructor.MainInitializer;

public class Main {

    public static void main(String[] args) {
        MainInitializer.init();
    }
}
