package constructor;

import entity.Port;
import entity.Ship;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class Context {
    private static volatile Context instance;

    public static Context getInstance(){
        if (instance == null){
            synchronized (Context.class) {
                if (instance == null) {
                    instance = new Context();
                }
            }
        }
        return instance;
    }

    private Context(){}

    public  Port port;
    public  int counter = 0;
    public ArrayList<Ship> waitingShips;
}
