package constructor;

import entity.Port;
import menu.MenuController;

import java.util.ArrayList;

public class MainInitializer {

    public static void init() {
        initContext();
        initMenu();
    }

    private static void initContext() {
        Context.getInstance().port = generatePort();
        Context.getInstance().waitingShips = new ArrayList<>();
    }

    private static void initMenu() {
        MenuController.getInstance().run();
    }

    private static Port generatePort() {
        Port port = new Port();
        port.setWaterCapacity(0);
        port.setShipsInside(new ArrayList<>());
        return port;
    }
}
