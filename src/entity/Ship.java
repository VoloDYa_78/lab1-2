package entity;

import constructor.Context;
import lombok.Getter;
import lombok.Setter;

public class Ship {

    @Getter @Setter
    private int id;

    @Getter @Setter
    private int capacity = 0;

    public Ship() {
        this.id = ++Context.getInstance().counter;
    }
}
