package entity;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class Port {

    private int waterCapacity;
    private ArrayList<Ship> shipsInside;

}
